FROM python:3.6-alpine

RUN apk add --no-cache git
WORKDIR /app/

RUN pip install pipenv
COPY Pipfile Pipfile.lock /app/
RUN pipenv install --dev --system

COPY . /app/

EXPOSE 4000

CMD gunicorn -w 2 -b 0.0.0.0:4000 app:app
