import pytest
import app


@pytest.fixture
def client():
    app.app.config['TESTING'] = True
    client = app.app.test_client()

    yield client


def test_version(client, monkeypatch):
    # mock the return value for the git command, otherwise we are just going to be reimplementing
    # the logic here
    monkeypatch.setattr('subprocess.check_output', lambda *args, **kwargs: b'fakesha')
    rv = client.get('/version')
    assert rv.json == {'git_revision': 'fakesha'}


def test_version_no_mock(client):
    rv = client.get('/version')
    assert rv.status_code == 200
