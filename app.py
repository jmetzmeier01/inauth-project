import subprocess
from flask import (
    Flask,
    jsonify,
)

app = Flask(__name__)


@app.route('/version')
def version():
    """
    Get the current version of the application

    :returns: The current git sha of HEAD for the running application
    """
    # gitpython is a bit overkill and it uses subprocess anyways
    sha = subprocess.check_output(['git', 'rev-parse', 'HEAD'], cwd=app.root_path).decode('utf-8').strip()
    return jsonify({
        'git_revision': sha
    })
